import axios from "axios"
import React, {Component} from 'react';
import Connections from "../Components/Connections"

class Home extends Component {
    render() {
        return (
            <div class="col-lg-12">
                <Connections/>
            </div>
        );
    }
}

export default Home;