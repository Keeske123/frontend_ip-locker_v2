import './App.css';
import Navbar from "./Components/Navbar";
import Home from "./Pages/Home";
import LoginPage from "./Pages/LoginPage";
import {
    Route,
    Switch
} from "react-router-dom";

function App() {
    return (

        <div class="col-lg-12">
            <Navbar/>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/LoginPage" component={LoginPage}/>
            </Switch>
        </div>
    );
}

export default App;
