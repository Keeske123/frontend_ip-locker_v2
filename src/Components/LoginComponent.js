import React from "react";
import { Link } from 'react-router-dom';

const Login = (props) => {
    return (
        <div className="login-page">
            <div className="form">
                <form className="login-form" onSubmit={props.loginUser}>
                    <input type="username" name="username" placeholder="Username" required={true}/>
                    <input type="password" name="password" placeholder="password" required={true}/>
                    <button type="Submit">Login</button>
                    <br/>
                </form>
            </div>
        </div>
    );
};

export default Login;