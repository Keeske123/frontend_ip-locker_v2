import React, {Component} from 'react';
import axios from "axios"
import Title from "./Title";

class Connections extends Component {

    state = {
        employees: [],
    };

    componentDidMount() {
        axios.get('http://localhost:8081/Employees/GetEmployees', function (req, res) {
            res.header("Access-Control-Allow-Origin", "http://localhost:3000")
        })
            .then(emp => {
                const employees = emp.data;
                this.setState({employees});
                console.log(employees)
            })
    }

    render() {
        return (
            <div>
                <Title title='Employee Connections'/>
                <div>
                    {this.state.employees.map(employees => {
                        return (
                            <ul class="list-inline">
                                <li>
                                <div class="row">
                                    <div class="EmployeeConnection col-lg-12">
                                        <div class="col-lg-5">
                                            <label className="">{employees.employeeName} | {employees.currentIp}</label>
                                        </div>
                                        <div class="col-lg-5">
                                            <button id="checkConnection">{employees.employeeID}</button>
                                        </div>

                                    </div>
                                </div>
                                </li>
                            </ul>
                        );
                    })}
                </div>
            </div>
        )
    };

}

export default Connections;