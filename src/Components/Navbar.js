import React, {Component} from 'react';
import {FaAlignRight} from "react-icons/fa";
import {Link} from "react-router-dom";

export default class Navbar extends Component {
    state = {
        isOpen: false
    };
    handleToggle = () => {
        this.setState({isOpen: !this.state.isOpen})
    };

    render() {

        function getCookie(name) {
            const re = new RegExp(name + "=([^;]+)");
            const value = re.exec(document.cookie);
            return (value != null) ? unescape(value[1]) : null;
        }

        if (getCookie("name") === null) {
            return (
                <nav className="navbar">
                    <div className="nav-center col-lg-8">
                        <div className="nav-header col-lg-2">
                            <button type="button" className="nav-btn" onClick={this.handleToggle}>
                                <FaAlignRight className="nav-icon"/>
                            </button>
                        </div>
                        <ul className={this.state.isOpen ? "nav-links show-nav" : "nav-links"}>
                            <li>
                                <div class="col-lg-3">
                                    <Link to="/">Home</Link>
                                </div>
                            </li>
                            <li className="right">
                                <div className="col-lg-3">
                                    <Link to="/loginPage">Login</Link>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            );
        } else {
            return (
                <nav className="navbar">
                    <div className="nav-center">
                        <div className="nav-header">
                            <button type="button" className="nav-btn" onClick={this.handleToggle}>
                                <FaAlignRight className="nav-icon"/>
                            </button>
                        </div>
                        <ul className={this.state.isOpen ? "nav-links show-nav" : "nav-links"}>
                            <li>
                                <Link to="/">Home</Link>
                                <li>
                                    <Link to="/">Home</Link>
                                </li>
                                <li className="right">
                                    <Link to="/">Logout</Link>
                                </li>
                            </li>
                        </ul>
                    </div>
                </nav>
            );
        }
    }
}